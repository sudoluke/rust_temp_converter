use std::io;

fn main() {
    println!("Temperature conversion");
    println!("Temperature as a decimal");
    // initialize empty string to capture tempurature to convert
    let mut temp = String::new(); 
    io::stdin().read_line(&mut temp).expect("Failed to read line");

    println!("Convert to: C/F");
    // initialize empty string to capture type to convert to
    let mut tpe = String::new();
    io::stdin().read_line(&mut tpe).expect("Failed to read line");
    // change temp to float
    let temp: f64 = temp.trim().parse().unwrap();
    // ensure string has no whitespace
    let tpe = tpe.trim().parse().expect("Please type C or F");
    // Call convert functions
    convert_temp(temp, tpe)
}

fn convert_temp(x: f64, t: String) {
    let f_to_c = 5 as f64 / 9 as f64;
    let c_to_f = 9 as f64 / 5 as f64;
    if t == "C" {
        println!("Converting {} Farenheit to Celsius", x);
        let temp = (x - 32.0) * f_to_c;
        println!("{} C", temp);
    } else if t == "F" {
        println!("Converting {} Celsius to Farenheit", x);
        let temp = (x * c_to_f) + 32.0;
        println!("{} F", temp);
    } else {
        println!("something didn't work");
    }
}
